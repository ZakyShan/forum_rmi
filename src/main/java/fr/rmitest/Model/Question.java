package fr.rmitest.Model;

import lombok.Data;

import java.util.Collection;

@Data
public class Question {

    private Collection<Response> responses;
    private String content;
    private User user;
    public Question(User user, String content)
    {
        this.user = user;
        this.content = content;
    }

    public void addResponse(Response response)
    {
        this.responses.add(response);
    }

    public void removeReponse(Response response)
    {
        this.responses.removeIf(r -> r.equals(response));
    }




}

package fr.rmitest.Service;

import fr.rmitest.Model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserFactory {

    private static UserFactory factory;

    private HashMap<String, User> users;

    private List<User> loggedUsers;

    public UserFactory() {
        this.users = new HashMap<>();
        this.loggedUsers = new ArrayList<>();
    }

    public static UserFactory getInstance() {
        if(UserFactory.factory == null)factory = new UserFactory();
        return UserFactory.factory;
    }

    public User registerUser(String username, String password) {
        User u = new User(username, password);
        this.users.put(username, u);
        return u;
    }

    public User validateCreditentials(String username, String password) {
        if(this.users.containsKey(username)) {
            if(this.users.get(username).getPassword().equals(password)) {
                return this.users.get(username);
            }
        }
        return null;
    }

    public void login(User u) {
        this.loggedUsers.add(u);
    }

    public void logout(User u) {
        this.loggedUsers.remove(u);
    }

}
